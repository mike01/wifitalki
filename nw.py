import threading
from queue import Queue, Empty
from multiprocessing import Pipe
import subprocess
import wave
import struct
import sys
import time
import random
from ctypes import *
from contextlib import contextmanager
import logging
import socket

import pyaudio
from Crypto.Cipher import AES

from pypacker.layer12.radiotap import Radiotap
from pypacker.layer12 import ieee80211
from pypacker import psocket, utils, ppcap
from pypacker.structcbs import unpack_I, pack_I
from pypacker import pypacker


time_time_int = lambda: int(time.time())

logger = logging.getLogger("wt")

MTU = 2300

# TODO: also in gui.py -> place in central config?
ROOM_NOFLAGS    = 0
ROOM_ENCRYPTED  = 1


def execute_cmd(cmd):
	resp = subprocess.getoutput(cmd)
	if len(resp) != 0:
		logger.debug("{cmd}: {resp}".format(cmd=cmd, resp=resp))


def prepare_interface_for_monitor(iface_name, channel=1):
	mode = utils.get_wlan_mode(iface_name)

	if mode == utils.WLAN_MODE_MONITOR:
		# Assume everything is ok, no additional configuration
		return

	monitor_active_question = "Interface %s is *not* in monitor mode. Should I now try to switch to monitor mode? (y/n)\n" % iface_name
	monitor_active_question += "*** WARNING: Network manager interfere with monitor mode -> make sure it is deactivated! ****\n"
	monitor_active_question += "Examples to deactivate Network manager on terminal:\n"
	monitor_active_question += "/etc/init.d/NetworkManager stop\n"
	monitor_active_question += "systemctl stop NetworkManager.service\n"

	answer = input(monitor_active_question)

	if answer != "y":
		# Monitor mode is needed, exit if isn't present
		logger.warning("Monitor mode is needed, exiting now")
		sys.exit(1)

	# TODO: requirements: iwconfig, iw
	# Deactivate and configure
	execute_cmd("ifconfig {iface_name} down".format(iface_name=iface_name))
	#execute_cmd("iw dev {iface_name} set monitor otherbss fcsfail".format(iface_name=iface_name))
	execute_cmd("iw dev {iface_name} set monitor otherbss".format(iface_name=iface_name))
	#execute_cmd("iw dev {iface_name} set monitor none".format(iface_name=iface_name)) # nok: more missed frames w/ NULL data
	execute_cmd("ifconfig {iface_name} mtu {mtu}".format(iface_name=iface_name, mtu=MTU))
	execute_cmd("iw dev {iface_name} set power_save off".format(iface_name=iface_name))
	execute_cmd("iwconfig {iface_name} rts off".format(iface_name=iface_name))
	## short: decreases amount of not received frames
	execute_cmd("iwconfig {iface_name} retry short 1".format(iface_name=iface_name))
	execute_cmd("iwconfig {iface_name} retry long 1".format(iface_name=iface_name))
	## Reactivate
	execute_cmd("ifconfig {iface_name} up".format(iface_name=iface_name))
	execute_cmd("iwconfig {iface_name} channel {channel}".format(iface_name=iface_name, channel=channel))
	execute_cmd("iw {iface_name} set bitrates".format(iface_name=iface_name))


def encrypt(data, key):
	cipher = AES.new(key, AES.MODE_GCM)
	ciphertext, tag = cipher.encrypt_and_digest(data)
	return ciphertext, cipher.nonce, tag


def decrypt(ciphertext, key, nonce, tag):
	# TODO: should raise an exception if tag does not match
	cipher = AES.new(key, AES.MODE_GCM, nonce)
	#return cipher.decrypt(ciphertext)
	return cipher.decrypt_and_verify(ciphertext, tag)

"""
KEY = b"\01" * 16
KEY2 = b"\02" * 16
DATA = b"x"*30
ciphertext, nonce, tag = encrypt(DATA, KEY)

for val in [ciphertext, nonce, tag]:
	print(val)
cleartext = decrypt(ciphertext, KEY, nonce, tag)
print("%s\n%s" % (DATA, cleartext))
sys.exit(1)
"""

FLAG_VERSION = 0x00
FLAG_ENCRYPTED = 0x01

class ChannelFrame(pypacker.Packet):
	__hdr__ = (
		# Bits: Version:4 | [ xxx | ENCRYPTED]
		("flags", "B", 0),
		("id", "I", 0),
		("cnt", "I", 0),
		("room", "16s", b"\x00" * 16),
		# Non-encrypted packet: flags|id|cnt|room|data
		# Encrypted packet: flags|id|cnt|room| nonce | tag | encr(ts|data)
	)

	def _get_encrypted(self):
		return 1 if (self.flags & FLAG_ENCRYPTED) == FLAG_ENCRYPTED else 0

	def _set_encrypted(self, value):
		self.flags = (self.flags & ~FLAG_ENCRYPTED) | (FLAG_ENCRYPTED if value == 1 else 0)

	encrypted = property(_get_encrypted, _set_encrypted)


	def _get_room(self):
		return self.room.rstrip(b"\x00").decode()

	def _set_room(self, room):
		self.room = room.ljust(16, "\x00").encode()

	room_s = property(_get_room, _set_room)

	def set_body_bytes_encrypted(self, key, data_plain):
		self.encrypted = 1
		ts = pack_I(time_time_int())
		data_encr, nonce, tag = encrypt(ts + data_plain, key)
		#logger.debug("Encrypting: key=%s, nonce: %d, plain bytes: %d" % (key, len(nonce), len(data_plain)))
		self.body_bytes = nonce + tag + data_encr

	def get_body_bytes_decrypted(self, key):
		if self.encrypted == 0:
			return None, self.body_bytes

		try:
			nonce, tag, data_encr = self.body_bytes[:16], self.body_bytes[16:32], self.body_bytes[32:]
			data = decrypt(data_encr, key, nonce, tag)
		except:
			# Not enough bytes, MAC check failed
			return None, None
		ts, data_plain = data[:4], data[4:]
		#logger.debug("Decrypting: key=%s, nonce: %d, plain bytes: %d" % (key, len(nonce), len(data_plain)))
		ts = unpack_I(ts)[0]
		return ts, data_plain


class TimeTracker(object):
	"""Tracks a timespan."""
	def __init__(self, allowed_diff_sec=60*5):

		if allowed_diff_sec < 10:
			allowed_diff_sec = 10

		self._allowed_diff_sec = allowed_diff_sec
		self._is_running = True
		self._time = time_time_int()
		self._incrementer_event = threading.Event()
		self._timethread = threading.Thread(target=TimeTracker.time_incrementer_cycler, args=[self])
		self._timethread.start()

	def get_time(self):
		return self._time

	def stop(self):
		self._is_running = False
		logger.debug("Setting event")
		try:
			self._incrementer_event.set()
		except:
			logger.debug("Event was released")
		logger.debug("Stop finished")

	@staticmethod
	def time_incrementer_cycler(obj):
		while obj._is_running:
			obj._time = time_time_int()
			is_set = obj._incrementer_event.wait(timeout=5.0)
			#logger.debug("Updating time")
			# Set was called before or while wait
			if is_set:
				obj._is_running = False
		logger.debug("Time cycler was stopped")

	def is_in_timewindow(self, seconds):
		#logger.debug("Time diff: %d <-> %d = %d" % (self._time, seconds, abs(self._time - seconds)))
		return abs(self._time - seconds) <= self._allowed_diff_sec

CHATROOM_NAME_PUBLIC = "Public"

class NWHandler(object):
	"""
	- Sends/receive audio frames including en/decrypting
	- Keeps track of chatrooms
	"""
	IEEE80211_ADDR1 = b"\x00"*5 + b"\x00"
	IEEE80211_ADDR2 = b"\x00"*5 + b"\x00"
	IEEE80211_ADDR3 = b"\x00"*5 + b"\x00"

	PKT_BASE_IEEE80211 = (Radiotap(present_flags=0x0) +\
		ieee80211.IEEE80211(type=ieee80211.DATA_TYPE,
		subtype=ieee80211.D_NULL,
		to_ds=0, from_ds=0,
		duration=0x0000,
		protected=1
		) +\
		ieee80211.IEEE80211.Dataframe(
		addr1=IEEE80211_ADDR1,
		addr2=IEEE80211_ADDR2,
		addr3=IEEE80211_ADDR3,
		seq_frag=0,
		qos_ctrl=None,
		sec_param=0x1337133813391340
	)).bin()

	# Sample bytes to send = MTU - [Metadata of IEEE80211] - [Meta data of Channelframe] - [buffer]
	PKT_MAX_PAYLOAD_BTS = (MTU - (len(PKT_BASE_IEEE80211) + 1+4+4+16 +16)) & (~0x1)
	PKT_MIN_BTS = (len(PKT_BASE_IEEE80211) + len(ChannelFrame(body_bytes=b"\x00").bin())) & (~0x1)

	def __init__(self, iface_name, cb_rcv, buffersize=4, resents=0, cb_update_rooms=lambda rooms: None):
		"""cb_rcv -- lambda id, room, data: xxx"""
		# TODO: can be tweaked, 4=OK
		self._is_running = True
		self._iface_name = iface_name
		self._buffersize = buffersize
		self._cb_rcv = cb_rcv
		self._sendouts = 1 + resents
		self._rcv_cycler = None
		self._timetracker = TimeTracker()
		self._cnt = 0
		self._sendbuffer = ([b""] * buffersize) + [0]

		self._cb_update_rooms = cb_update_rooms
		#self._room = b"\x00" * 16
		self.set_room(CHATROOM_NAME_PUBLIC)
		# {b"roomname" : [FLAGS]}
		self._rooms_seen = {}
		self._key = None
		self._id = int(random.random() * 2**32) & 0xFFFFFFFF
		self._id__cnt_drops = {}
		logger.debug("Client id: %X"% self._id)

		# TODO: too small timeout?
		self._sock = psocket.SocketHndl(iface_name=self._iface_name, timeout=1)
		self._rcv_cycler_th = threading.Thread(target=NWHandler._rcv_cycler, args=[self])
		self._rcv_cycler_th.start()

		logger.debug("Init finished")

	def get_nw_info(self):
		"""return -- {b"room1"}, senders"""
		return set(self._rooms_seen), len(self._id__cnt_drops)

	def clear_roominfo(self):
		self._rooms_seen.clear()

	@staticmethod
	def rtap_get_cf_frame(rtap1):
		# TODO: update
		ieee1 = rtap1.higher_layer

		if ieee1.protected == 0 or ieee1.higher_layer is None:
			return None

		"""
		if ieee1 is None or\
			ieee1.type != ieee80211.DATA_TYPE or\
			ieee1.subtype != ieee80211.D_NULL or\
			ieee1.to_ds != 0 or\
			ieee1.from_ds != 0:
			return None
		"""
		data1 = ieee1.higher_layer

		if data1.sec_param != 0x1337133813391340:
			return None

		"""
		if data1 is not None and\
			data1.addr1 == NWHandler.IEEE80211_ADDR1 and\
			data1.addr2 == NWHandler.IEEE80211_ADDR2 and\
			data1.addr3 == NWHandler.IEEE80211_ADDR3:

		"""
		return ChannelFrame(data1.body_bytes)
		"""
		return None
		"""

	def set_key(self, key):
		# None=disable encryption
		if key is not None and len(key) != 16:
			logger.warning("Invalid key: %s, length: %d" % (key, len(key)))
			return

		logger.debug("Setting key %s and clearing sendbuffer" % key)
		self._key = key
		self._clear_sendbuffer()

	def set_room(self, room):
		# Convert to bytes representation
		room = room[:16].ljust(16, "\x00").encode()
		self._room = room
		self._clear_sendbuffer()

	@staticmethod
	def _rcv_cycler(obj):
		rtap_get_cf_frame = NWHandler.rtap_get_cf_frame
		obj_sock_recv = obj._sock.recv
		# id : [cnt, drops]
		id__cnt_drops = obj._id__cnt_drops

		while obj._is_running:
			try:
				# TODO: takes quite some CPU (500pkt/s -> +1%)
				bts = obj_sock_recv()

				if len(bts) < NWHandler.PKT_MIN_BTS:
					continue

				# TODO: takes quite some CPU (peaks: 500pkt/s -> +16%)
				rtap1 = Radiotap(bts)
				cf1 = rtap_get_cf_frame(rtap1)
			except Exception as ex:
				logger.debug(ex)
				# Assume timeout or socket is closed
				continue

			if cf1 is None:
				continue

			# TODO: more performant: room instead of room_s
			if cf1.room_s not in obj._rooms_seen:
				obj._rooms_seen[cf1.room_s] = [ROOM_NOFLAGS if cf1.encrypted == 0 else ROOM_ENCRYPTED]
				logger.debug("Found new room: %s" % cf1.room_s)
				obj._cb_update_rooms(obj._rooms_seen)

			# Discard if: Encrypted packet and no key, not encrypted packet and key is set, not our room
			if (cf1.encrypted == 1 and obj._key is None) or (cf1.encrypted == 0 and obj._key is not None) or cf1.room != obj._room:
				#logger.debug("Invalid packet: type=%s" % type(cf1))
				continue

			# Ignore frames sent out by us (reflected)
			if cf1.id == obj._id:
				continue

			ts, data = cf1.get_body_bytes_decrypted(obj._key)

			if (ts is not None and not obj._timetracker.is_in_timewindow(ts)) or data is None:
				# Packet is out of time window, replayed?
				# TODO: does not work
				logger.debug("Invalid data or out of timewindow: %d, data=%s" % (ts, data))
				continue

			if cf1.id in id__cnt_drops:
				cnt_exptected = id__cnt_drops[cf1.id][0] + 1
			else:
				id__cnt_drops[cf1.id] = [cf1.cnt, 0]
				cnt_exptected = cf1.cnt

			# Happens on resent frames
			# WARNING: too many resents will increase delay
			if cf1.cnt < cnt_exptected:
				#logger.debug("cf1.cnt < cnt_next: %d < %d" % (cf1.cnt, cnt_next))
				continue

			if cf1.cnt > cnt_exptected:
				cnt_diff = cf1.cnt - cnt_exptected
				id__cnt_drops[cf1.id][1] += cnt_diff
				logger.debug("Lost %d packets for %X (exptected: %d, current: %d)" % (cnt_diff, cf1.id, cnt_exptected, cf1.cnt))
				# TODO: Strange errors, handle this
				if cnt_diff > 1000:
					logger.debug("Large diff: %d > 1000" % cnt_diff)
					#continue

			id__cnt_drops[cf1.id][0] = cf1.cnt
			obj._cb_rcv(cf1.id, cf1.cnt, cf1.room_s, data)
			#logger.debug("Received %d bytes of audio" % len(data))
		logger.debug("Recv cycler finished")

	def stop(self):
		self._is_running = False
		#self._sock._socket.shutdown(socket.SHUT_RD)
		#self._sock._socket.setblocking(False)
		#self._sock._socket.settimeout(0)
		#
		self._sock.close()
		self._sock = None
		logger.debug("Joining rcv cycler")
		# TODO: This takes long
		self._rcv_cycler_th.join()
		self._rcv_cycler_th = None
		self._timetracker.stop()

	def set_channel(self, channel):
		utils.switch_wlan_channel(self._iface_name, channel)
		self._clear_sendbuffer()

	def _clear_sendbuffer(self):
		self._sendbuffer[-1] = 0

	def send(self, data):
		# data too large
		if len(data) > NWHandler.PKT_MAX_PAYLOAD_BTS:
			logger.debug("Packet too large: %d > %d, splitting up..." % (len(data), NWHandler.PKT_MAX_PAYLOAD_BTS))
			# Packet too large: recursive call with smaller packets
			for bts in [data[off: off + NWHandler.PKT_MAX_PAYLOAD_BTS] for off in range(0, len(data), NWHandler.PKT_MAX_PAYLOAD_BTS)]:
				self.send(bts)
			return

		sb = self._sendbuffer
		# Buffer data: BUFFER_SIZE*512 Bytes
		sb[sb[-1]] = data
		sb[-1] += 1

		if sb[-1] < self._buffersize:
			return

		# Enough buffered, send out
		data = b"".join(sb[0: self._buffersize])
		sb[-1] = 0

		# Create Packet to send out
		self._cnt = (self._cnt + 1) & 0xFFFFFFFF
		cf1 = ChannelFrame(id=self._id, cnt=self._cnt, room=self._room, body_bytes=data)

		if self._key is not None:
			#logger.debug("Sending encrypted")
			cf1.set_body_bytes_encrypted(self._key, data)

		pkt_bts = NWHandler.PKT_BASE_IEEE80211 + cf1.bin()

		#logger.debug("Sending %d bytes of audio" % len(data))
		#rtp = "%s" % Radiotap(pkt_bts)
		#logger.debug(rtp + "\nlength=%d" % len(pkt_bts))
		# Resend to compensate packet drops
		# TODO: remove, just for testing
		#for _ in range(3):
		#for _ in range(2):
		for _ in range(self._sendouts):
			self._sock.send(pkt_bts)





"""
utils.set_interface_mode("wls1", monitor_active=True, state_active=True)

cf1 = ChannelFrame()
def cb1(id, room, data):
	print("%d %s %s" % (id, room, data))
nw1 = NWHandler("wls1", cb1)
nw1.set_channel(11)

logger.debug("Sending data")
nw1.send(123, "test123", b"somedata")

nw1.set_key(b"x" * 16)
nw1.send(123, "test123", b"somedata")

nw1.stop()


utils.set_interface_mode("wls1", monitor_active=False, state_active=True)
"""

"""
writer = ppcap.Writer(filename="packets_rtap_ieee_base_wt.pcap", linktype=ppcap.DLT_IEEE802_11_RADIO)
print(NWHandler.PKT_BASE_PKT)
writer.write(NWHandler.PKT_BASE_PKT)
writer.close()
"""


"""
iface = sys.argv[1]

prepare_interface_for_monitor(iface)

def cb1(id, room, data):
	print("ID=%d, room=%s, data=%s" % (id, room, data))


print("Init NWHandler")
nw1 = NWHandler(iface, cb1)
nw1.set_channel(3)
nw1.set_room("Test123")

while True:
	data = input("Enter data:")
	print("Sending: %s" % data)
	nw1.send(data.encode())
nw1.stop()
"""
