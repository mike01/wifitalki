# Wifitalki
This is a walkitalki application using wifi monitor mode (no internet needed).

# Installation
 * Install pypacker: https://gitlab.com/mike01/pypacker
 * Install pyalsaaudio: https://github.com/larsimmisch/pyalsaaudio

# Usage
 * Start on two separated machines (check aplay -L/arecord -L for correct device)
   * python wt.py -i wlp2s0 -d sysdefault -e sysdefault --logs logs.txt
 * Local testing: only sending
   * python wt.py -i wlp2s0 -d sysdefault -e sysdefault --logs logs.txt --norcv
 * Local testing: only receiving
   * python wt.py -i wlp0s29f7u1 -d sysdefault -e sysdefault --logs logs2.txt --nosend

![alt text](screenshot1.png "Wifitalki screenshot")

# Todo
- Identify and alert dropped packets. Works with realtime?
- Relaying of packets. Based on signal strength?
