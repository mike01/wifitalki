import threading
# WARNING: max mount of storable data is limited in Pipe! eg 16*2500bytes
from multiprocessing import Queue
import wave
import struct
import sys
import time
from ctypes import *
from contextlib import contextmanager
import logging

import alsaaudio
from Crypto.Cipher import AES

from pypacker.layer12.radiotap import Radiotap
from pypacker.layer12.ieee80211 import IEEE80211
from pypacker.structcbs import unpack_I

time_time = time.time
time_sleep = time.sleep

logger = logging.getLogger("wt")


# Loudness level threshold at wich frames get transmitted. Everything beneath gets discarded.
AUDIO_THRESHOLD	= 20
AUDIO_PERIODSIZE = 100
#AUDIO_PERIODSIZE = 160
AUDIO_FORMAT = alsaaudio.PCM_FORMAT_S16_LE
#FORMAT = alsaaudio.PCM_FORMAT_S8 # quite noisy
AUDIO_CHANNELS = 1
#AUDIO_RATE = 8000
AUDIO_RATE = 12000
#AUDIO_RATE = 32000
#AUDIO_RATE = 44100
#
# Config examples
# rate=12000, S16_LE
# 256 samples, 512 bytes

class OutputMerger(object):
	OUTPUTSTREAMS_MAX = 50
	# Amount of seconds of inactivity until output stream gets closed
	QUEUE_TIMEOUT = 60*3

	def __init__(self, device_name):
		self._device_name = device_name
		# {id : [stream, frames_pipe, output_thread]}
		self._id_streampipethread = {}
		self._is_running = True


	def queue_to_output(self, id, frames):
		if not self._is_running:
			return

		if id not in self._id_streampipethread:
			logger.debug("Creating new output stream")

			if len(self._id_streampipethread) > OutputMerger.OUTPUTSTREAMS_MAX:
				logger.warning("Warning, too many output streams (%d reached)" % OutputMerger.OUTPUTSTREAMS_MAX)
				return

			frames_q = Queue()
			# Create output device
			dev_out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, device=self._device_name)
			dev_out.setchannels(AUDIO_CHANNELS)
			dev_out.setrate(AUDIO_RATE)
			dev_out.setformat(AUDIO_FORMAT)
			dev_out.setperiodsize(AUDIO_PERIODSIZE)

			cycler_out_th = threading.Thread(
				target=OutputMerger._output_cycler,
				args=[self, id, dev_out, frames_q]
			)
			self._id_streampipethread[id] = [dev_out, frames_q, cycler_out_th]
			cycler_out_th.start()

		self._id_streampipethread[id][1].put(frames)

	@staticmethod
	def _output_cycler(obj, id, stream, frames_q):
		logger.debug("Started output cycler for id %X" % id)
		q_get = frames_q.get
		q_empty = frames_q.empty
		q_size = frames_q.qsize
		stream_write = stream.write

		try:
			while obj._is_running:
				if q_size() == 0:
					# Prebuffer if in-buffer is empty
					logger.debug("Buffering after received")
					#data = [q_get() for _ in range(10)]
					data = [q_get() for _ in range(2)]
					data = b"".join(data)
					stream.write(data)

				stream_write(q_get())
		except (EOFError, OSError):
			# Assume shutdown
			pass
		logger.debug("Output cycler %s finished" % id)

	def _shutdown_stream(self, id):
		"""Shutdown and delete stream with the given id"""
		logger.debug("Shutting down stream %s" % id)
		streampipethread = self._id_streampipethread[id]
		#logger.debug("Waiting to join stream thread")
		#streampipethread[2].join()
		# Now it's save to stop/close stream
		logger.debug("Stopping stream")
		#streampipethread[0].stop_stream()
		logger.debug("Closing stream")
		#streampipethread[0].close()
		# TODO: is this needed?
		logger.debug("Closing queue")
		streampipethread[1].close()
		del self._id_streampipethread[id]


	def stop(self):
		logger.debug("Stopping odm")
		self._is_running = False

		for id in list(self._id_streampipethread.keys()):
			self._shutdown_stream(id)
		logger.debug("Finished stopping streams")

def save_to_wave(audioframes_bts, filename, channels=1, rate=44100, format=alsaaudio.PCM_FORMAT_S16_LE):
	wavfile = wave.open(filename, "wb")
	wavfile.setnchannels(channels)
	wavfile.setsampwidth(audio.get_sample_size(format))
	wavfile.setframerate(rate)
	wavfile.writeframes(audioframes_bts)
	wavfile.close()


unpack_val = struct.Struct("<h").unpack

class AudioIn(object):
	def __init__(self, device_name, cb_frames_recorded, threshold=20):
		self._cb_frames_recorded = cb_frames_recorded
		self._threshold = threshold

		logger.debug("Starting input stream")
		dev_in = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, device=device_name)
		dev_in.setchannels(AUDIO_CHANNELS)
		dev_in.setrate(AUDIO_RATE)
		dev_in.setformat(AUDIO_FORMAT)
		dev_in.setperiodsize(AUDIO_PERIODSIZE)
		self._dev_in = dev_in

		self._is_running = True
		self._input_cycler = threading.Thread(target=AudioIn._input_cycler, args=[self])
		self._input_cycler.start()
		logger.debug("Input stream started")

	def set_threshold(self, threshold):
		self._threshold = threshold

	def stop(self):
		logger.debug("Stopping audio in")
		self._is_running = False
		self._dev_in.close()

	def _input_cycler(obj):
		cb_frames_recorded = obj._cb_frames_recorded
		dev_in_read = obj._dev_in.read

		try:
			while obj._is_running:
				sample_cnt, samples = dev_in_read()

				if sample_cnt > 0:
					ints_sub = [abs(unpack_val(samples[off: off+2])[0]) for off in range(0, len(samples), 20)]
					avg = sum(ints_sub) / len(ints_sub)
					cb_frames_recorded(samples, avg, avg > obj._threshold)
		except alsaaudio.ALSAAudioError:
			# Happens on: closing in-stream -> dev_in_read()
			pass
