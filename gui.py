import curses
import signal
import linecache
import time
import threading
import math
from curses.textpad import Textbox, rectangle
from collections import OrderedDict
import logging
import sys
import hashlib
import locale
from base64 import b64encode, b64decode

cu_cp = curses.color_pair

logger = logging.getLogger("wt")


ROOM_NOFLAGS	= 0
ROOM_ENCRYPTED	= 1

def get_normalized_aes_key(key_in):
	hl = hashlib.sha256()
	hl.update(key_in.encode())
	return hl.digest()[:16]

def get_vartracker(varname, initvalue):
	"""
	obj.varname = 123
	is_dirty = obj.varname_dirty
	"""
	varname_shadowed = "__%s" % varname
	varname_dirty = "%s_dirty" % varname_shadowed


	def get_val(obj):
		try:
			return obj.__getattribute__(varname_shadowed)
		except:
			obj.__setattr__(varname_shadowed, initvalue)
			return initvalue

	def set_val(obj, value):
		obj.__setattr__(varname_dirty, True)
		obj.__setattr__(varname_shadowed, value)

	def get_dirty(obj):
		try:
			dirty = obj.__getattribute__(varname_dirty)

			if dirty:
				# Reset to not dirty
				obj.__setattr__(varname_dirty, False)
			return dirty
		except:
			# Not yet set, not dirty
			obj.__setattr__(varname_dirty, False)
		return False

	return property(get_val, set_val), property(get_dirty)

class GUI(object):
	FILENAME_CONFIG = "config.txt"
	CHATROOM_NAME_PUBLIC = "Public"

	_miclevel_percent, _miclevel_percent_dirty = get_vartracker("miclevel_percent", 0.8)
	_nwinfo, _nwinfo_dirty = get_vartracker("nwinfo", 0)
	_chatrooms_cnt, _chatrooms_cnt_dirty = get_vartracker("chatrooms_cnt", 1)

	def __init__(self, initial_channel=3, cb_switch_channel=lambda channel: None,
			cb_switch_room=lambda roomname, key: None,
			cb_update_miclevel_send_percent=lambda milevel: None,
			cb_get_nw_info=lambda: (set(), 0)):
		self._cb_switch_channel = cb_switch_channel
		self._cb_switch_room = cb_switch_room
		self._cb_update_miclevel_send_percent = cb_update_miclevel_send_percent
		self._cb_get_nw_info=cb_get_nw_info

		#self._miclevel_percent = 0.8
		self._miclevel_send_percent = 0.05
		self._senders_seen = 0
		self._invalid_packets = 0

		# {chatroom : password}
		self._chatroom_key = {}
		# Current list of "seen" chatrooms
		"""
		self._chatrooms = OrderedDict({
			"someRoom123" : [ROOM_NOFLAGS],
			"Room1" : [ROOM_ENCRYPTED],
			"Room2" : [ROOM_ENCRYPTED],
			"Room3" : [ROOM_NOFLAGS],
			"xXx" : [ROOM_ENCRYPTED],
			"aTestroom1" : [ROOM_NOFLAGS],
			"aTestroom2" : [ROOM_NOFLAGS],
			"anotherTestroom" : [ROOM_NOFLAGS]
		})
		"""
		self._chatrooms = OrderedDict({GUI.CHATROOM_NAME_PUBLIC : [ROOM_NOFLAGS]})
		# [name, flags]
		self._chatroom_joined = [GUI.CHATROOM_NAME_PUBLIC, ROOM_NOFLAGS]
		self._channel_selected = initial_channel
		self._redraw_main_needed = True
		self._is_running = True
		self._thread_redraw = None
		self._load_config()

	def start(self):
		self._thread_curseswrapper = threading.Thread(target=self._curses_wrapper_wrapper)
		self._thread_curseswrapper.start()
		# TODO: curses needs some time to start, cancel before that will lead to crippled console
		#time.sleep(0.5)

	def _curses_wrapper_wrapper(self):
		curses.wrapper(self._curses_mainloop)
		self._is_running = False

	def _curses_mainloop(self, stdscr):
		self._stdscr = stdscr

		curses.noecho()
		# No "enter" needed to send keys
		curses.cbreak()
		# Hide cursor
		curses.curs_set(0)
		# Keys: Nice values instead escape sequences (eg curses.KEY_LEFT)
		stdscr.keypad(True)
		# Stop curses from using insert and deletion operations to update the screen
		stdscr.idcok(False)
		stdscr.idlok(False)

		# Color definitions
		curses.start_color()
		curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
		self.format_std_white = cu_cp(1)
		curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
		self.format_menuheader = cu_cp(2)
		curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
		self.format_std = cu_cp(3)
		curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_GREEN)
		self.format_std_inv = cu_cp(4)
		curses.init_pair(5, curses.COLOR_RED, curses.COLOR_BLACK)
		self.format_attention = cu_cp(5)

		stdscr.bkgd(self.format_std)
		stdscr.refresh()
		# Elements
		# newwin(height, width, start_y, start_x)
		height, width = stdscr.getmaxyx()

		self._win_menu = stdscr.subwin(1, 84, 0, 0)
		self._win_micsense = stdscr.subwin(3, 42, 2, 0)
		self._win_info = stdscr.subwin(5, 42, 5, 0)
		height_chatrooms = height - 2
		self._win_chatrooms = stdscr.subwin(height_chatrooms, 35, 2, 42)

		def resize_handler(signum, frame):
			self._redraw_mainscreen(force_refresh=True)
		# TODO: ValueError: signal only works in main thread
		#signal.signal(signal.SIGWINCH, resize_handler)

		self._thread_redraw = threading.Thread(target=GUI._redraw_as_needed_cycler, args=[self])
		self._thread_redraw.start()

		while self._is_running:
			try:
				key = self._stdscr.getch()
				self._redraw_main_needed = self._handle_key(key)
			except Exception as ex:
				logger.debug("%s" % ex)
				self._is_running = False


	def waitforexit(self):
		#while self._is_running:
		try:
			self._thread_curseswrapper.join()
		except:
			self._is_running = False

		self._save_config()

		"""
		try:
			print("Joining redraw")
			# Could be none if stopped too early
			self._thread_redraw.join()
		except:
			pass
		"""

	def _stop(self):
		self._is_running = False
		#curses.endwin()
		#self._stdscr.nodelay(True)
		"""
		print("Joining wrapper")
		self._thread_curseswrapper.join(timeout=0.5)

		print("Stop finished")
		self._stdscr.insertln()
		curses.ungetch("\n")
		"""

	@staticmethod
	def _redraw_as_needed_cycler(obj):
		obj._redraw_mainscreen()
		cnt = 0
		crooms = OrderedDict()

		while obj._is_running:
			if obj._redraw_main_needed:
				obj._redraw_main_needed = False
				obj._redraw_mainscreen()

			if obj._miclevel_percent_dirty:
				obj._draw_micsense()

			if obj._nwinfo_dirty:
				obj._draw_info()

			if obj._chatrooms_cnt_dirty:
				obj._draw_chatrooms()
			time.sleep(0.2)


	def _load_config(self):
		"""
		Config content:
		mic_threshold=0.1
		key_someroom1=test123
		key_someroom2=test123
		"""
		#logger.debug("Loading config from %s" % GUI.FILENAME_CONFIG)
		try:
			config_lines = open(GUI.FILENAME_CONFIG, "r").read().split("\n")

			for line in config_lines:
				if len(line) < 3 or "=" not in line:
					continue
				key, value = line.split("=", 1)

				if key == "mic_threshold":
					self._miclevel_send_percent = float(value)
				elif key.startswith("key_"):
					# pw: str/base64 -> bytes
					self._chatroom_key[key[4:]] = b64decode(value)
		except Exception as ex:
			# Config not readable
			#logger.warning("Could not read config from %s" % GUI.FILENAME_CONFIG)
			logger.debug(ex)

	def _save_config(self):
		#logger.debug("Saving config to %s" % GUI.FILENAME_CONFIG)

		try:
			fd_out = open(GUI.FILENAME_CONFIG, "w")
			fd_out.write("mic_threshold=%f\n" % self._miclevel_send_percent)

			for room, key in self._chatroom_key.items():
				# pw: bytes -> str/base64
				fd_out.write("key_%s=%s\n" % (room, b64encode(key).decode()))
			fd_out.close()
		except Exception as ex:
			#logger.warning("Could not write config to %s" % GUI.FILENAME_CONFIG)
			#logger.exception(ex)
			pass


	# Callback
	def update_miclevel(self, miclevel_percent):
		if miclevel_percent > 1.0:
			miclevel_percent = 1.0
		elif miclevel_percent < 0.0:
			miclevel_percent = 0
		self._miclevel_percent = miclevel_percent

	# Callback
	def update_nw_info(self, senders_seen, invalid_packets):
		self._invalid_packets = invalid_packets
		self._senders_seen = senders_seen
		self._nwinfo = True

	# Callback
	def update_chatrooms(self, newrooms):
		"""Replace seen chatrooms with newrooms"""
		# Independent copy
		newrooms = OrderedDict(newrooms)
		# Add currently joined chatroom. Handle public chatroom.
		newrooms[self._chatroom_joined[0]] = [self._chatroom_joined[1]]

		# Re-add public room
		#if GUI.CHATROOM_NAME_PUBLIC not in newrooms:
		newrooms[GUI.CHATROOM_NAME_PUBLIC] = [ROOM_NOFLAGS]

		self._chatrooms.clear()
		self._chatrooms.update(newrooms)
		self._chatrooms_cnt = len(self._chatrooms)

	def _draw_menu(self):
		win = self._win_menu
		win.erase()
		win.refresh()
		off_x = 0
		#font_u_b = curses.A_UNDERLINE | curses.A_BOLD

		win.bkgd(self.format_std)
		win.addstr(0, off_x, "F1", self.format_std_white)
		win.addstr(0, off_x+2, "MIC threshold down", self.format_std_inv)
		off_x += 3+17
		win.addstr(0, off_x, "F2", self.format_std_white)
		win.addstr(0, off_x+2, "MIC threshold up", self.format_std_inv)
		off_x += 3+15
		win.addstr(0, off_x, "F3", self.format_std_white)
		win.addstr(0, off_x+2, "Channel", self.format_std_inv)
		off_x += 3+6
		win.addstr(0, off_x, "F4", self.format_std_white)
		win.addstr(0, off_x+2, "Join room", self.format_std_inv)
		off_x += 3+8
		win.addstr(0, off_x, "F5", self.format_std_white)
		win.addstr(0, off_x+2, "Create room", self.format_std_inv)
		off_x += 3+10
		win.addstr(0, off_x, "F6", self.format_std_white)
		win.addstr(0, off_x+2, "Key", self.format_std_inv)
		off_x += 3+2
		win.addstr(0, off_x, "Esc", self.format_std_white)
		win.addstr(0, off_x+3, "Exit", self.format_std_inv)
		win.refresh()

	def _draw_micsense(self):
		win = self._win_micsense
		height, width = win.getmaxyx()
		# Don't use full width as there are borders
		width -= 3
		level_draw = int(self._miclevel_percent * width)
		level_send = int(self._miclevel_send_percent * width)

		win.erase()
		win.box()

		off_x = 0
		font_b = curses.A_BOLD
		win.bkgd(self.format_std)
		win.addstr(0, off_x, "MIC input (|=send threshold)", self.format_menuheader)

		level_str = ["*"]*level_draw + [" "]*(width - level_draw + 1)
		level_str[level_send] = "|"

		win.addstr(1, off_x+1, "".join(level_str), self.format_std)
		win.refresh()

	def get_user_input_line(self, win_parent, y_off_main, x_off_main, message):
		h_dialogue = 6
		w_dialogue = max([40, len(message) + 3])

		h_subwin = h_dialogue-3
		w_subwin = w_dialogue-4

		#mainwin = win_parent.subwin(h_dialogue, w_dialogue, y_off_main, x_off_main)
		mainwin = curses.newwin(h_dialogue, w_dialogue, y_off_main, x_off_main)
		mainwin.erase()
		mainwin.bkgd(self.format_std)
		mainwin.box()

		mainwin.addstr(1, 1, message)

		editwin = mainwin.subwin(1, w_subwin, y_off_main +3, x_off_main +2)
		rectangle(mainwin, 2, 1, 4, w_subwin+2)
		mainwin.refresh()

		box = Textbox(editwin, insert_mode=True)
		box.stripspaces = 1

		def validate(key):
			if key in [0x00, 0x0A]:
				return 0x07
			return key

		#retval = box.edit(validate)
		retval = box.edit()
		# TODO: hotfix for trailing 0x00
		retval = retval.strip().rstrip("\x00")
		mainwin.erase()
		mainwin.refresh()
		return retval

	def get_user_input_selection(self, win_parent, y_off, x_off, header, choices):
		choices_total = len(choices)
		rows_per_page = 10
		max_choicelen = max([len(str) for str in choices])
		position_idx = 0
		page_idx = 0
		pages_max = int(math.ceil(choices_total / rows_per_page))
		text_normal = self.format_std
		text_highlight = self.format_std_inv

		mainwin_width = max(max_choicelen + 4, len(header))
		mainwin = curses.newwin(rows_per_page + 2 + 3, mainwin_width + 4, y_off, x_off)
		mainwin.bkgd(self.format_std)
		mainwin.keypad(1)
		mainwin.addstr(1, 1, header, text_normal)
		mainwin.refresh()

		choicewin = mainwin.subwin(rows_per_page + 2, max_choicelen + 4, y_off+2, x_off+1)
		choicewin.box()
		choicewin.refresh()


		def get_page_by_choice_idx(choice_idx):
			return math.floor(choice_idx/rows_per_page)

		kcode = 0

		while kcode != 27:
			choicewin.erase()
			mainwin.border(0)
			choicewin.border(0)

			choice_start_idx = rows_per_page * page_idx
			choice_end_idx = choice_start_idx + rows_per_page - 1

			if choice_end_idx > choices_total - 1:
				choice_end_idx = choices_total - 1

			off_top = 0

			for idx in range(choice_start_idx, choice_end_idx + 1):
				if idx == position_idx:
					choicewin.addstr(1 +off_top, 2, choices[idx], text_highlight)
				else:
					choicewin.addstr(1 +off_top, 2, choices[idx], text_normal)
				off_top += 1

			mainwin.refresh()
			choicewin.refresh()

			kcode = mainwin.getch()

			if kcode == curses.KEY_DOWN:
				if position_idx < choices_total - 1:
					position_idx += 1
					page_idx = get_page_by_choice_idx(position_idx)
			elif kcode == curses.KEY_UP:
				if position_idx > 0:
					position_idx -= 1
					page_idx = get_page_by_choice_idx(position_idx)
			elif kcode == curses.KEY_LEFT:
				if page_idx > 0:
					page_idx -= 1
					position_idx = rows_per_page * page_idx
			elif kcode == curses.KEY_RIGHT:
				if page_idx < pages_max - 1:
					page_idx += 1
					position_idx = rows_per_page * page_idx
			elif kcode == b"\n"[0]:
				return position_idx
		choicewin.erase()
		mainwin.erase()
		choicewin.refresh()
		mainwin.refresh()

	def _draw_info(self):
		win = self._win_info
		win.erase()
		win.box()

		off_x = 0
		font_u_b = curses.A_UNDERLINE | curses.A_BOLD
		win.bkgd(self.format_std)
		win.addstr(0, off_x, "Network info", self.format_menuheader)
		win.addstr(1, off_x+1, "Channel: %d" % self._channel_selected, self.format_std)
		win.addstr(2, off_x+1, "Senders last seen: %d" % self._senders_seen, self.format_std)
		win.addstr(3, off_x+1, "Invalid packets received: %d" % self._invalid_packets, self.format_std)
		win.refresh()

	def _draw_chatrooms(self):
		# TODO: handle too many rooms (limit? scrolling?)
		# Independent copy
		chatrooms = OrderedDict(self._chatrooms)
		win = self._win_chatrooms
		_, w = win.getmaxyx()
		win.erase()
		win.refresh()
		win.resize(len(self._chatrooms)+3, w)
		win.box()

		win.bkgd(self.format_std)
		win.addstr(0, 0, "Chatrooms (e/E=Encrypted)", self.format_menuheader)
		chatroom_names_sorted = sorted(list(chatrooms.keys()))

		for idx, roomname in enumerate(chatroom_names_sorted):
			strformat = self.format_std if roomname != self._chatroom_joined[0] else self.format_std_inv
			flagstr = ""
			# Public chatroom gets excluded here as it doesn't hav any flags; won't match here
			if roomname in chatrooms and chatrooms[roomname][0] == ROOM_ENCRYPTED:
				if roomname in self._chatroom_key:
					flagstr = "[E]"
				else:
					flagstr = "[e]"
			win.addstr(1+idx, 1, "%-3s %s" % (flagstr, roomname), strformat)
		win.refresh()

	def _reset_rooms(self):
		# Switch to default room
		self._chatrooms.clear()
		self._chatrooms[GUI.CHATROOM_NAME_PUBLIC] = [ROOM_NOFLAGS]
		self._chatroom_joined[0] = GUI.CHATROOM_NAME_PUBLIC
		self._chatroom_joined[1] = ROOM_NOFLAGS

	def _handle_key(self, key):
		MICLEVEL_STEP = 0.05
		dialogue_y = 10
		dialogue_x = 0

		if key == curses.KEY_F1:
			newval = self._miclevel_send_percent - MICLEVEL_STEP

			if newval < 0:
				newval = 0
			self._miclevel_send_percent = newval
			self._draw_micsense()
			self._cb_update_miclevel_send_percent(newval)
		elif key == curses.KEY_F2:
			newval = self._miclevel_send_percent + MICLEVEL_STEP

			if newval > 1:
				newval = 1

			self._miclevel_send_percent = newval
			self._draw_micsense()
			self._cb_update_miclevel_send_percent(newval)
		elif key == curses.KEY_F3:
			choices = ["%d" % val for val in range(1, 12)]
			sel = self.get_user_input_selection(self._stdscr, dialogue_y, 0, "Select a channel:", choices)
			self._channel_selected = int(choices[sel])
			self._reset_rooms()
			self._cb_switch_channel(self._channel_selected)
			self._cb_switch_room(GUI.CHATROOM_NAME_PUBLIC, None)
			return True
		elif key == curses.KEY_F4:
			rooms_copy_d = OrderedDict(self._chatrooms)
			roomnames = sorted(list(self._chatrooms.keys()))
			roomnames_idx = self.get_user_input_selection(self._stdscr, dialogue_y, 0, "Select a chatroom:", roomnames)

			logger.debug("Getting room with idx %d" % roomnames_idx)
			#chatroom_name = GUI.CHATROOM_NAME_PUBLIC
			chatroom_name = roomnames[roomnames_idx]
			#flags = ROOM_NOFLAGS
			flags = rooms_copy_d[chatroom_name][0]
			key = None

			logger.debug("Checking room flags")
			# Encryption needed but no key known so far
			if flags == ROOM_ENCRYPTED:
				key = self._chatroom_key.get(chatroom_name, None)

				if key is None:
					key = self.get_user_input_line(self._stdscr, dialogue_y, 0, "Password needed:")

					if len(key) == 0:
						return True

					key = get_normalized_aes_key(key)
					self._chatroom_key[chatroom_name] = key

				logger.debug("Key is: %s" % key)

			self._chatroom_joined[0] = chatroom_name
			self._chatroom_joined[1] = flags
			logger.debug("Will now switch room")
			self._cb_switch_room(chatroom_name, key)
			return True
		elif key == curses.KEY_F5:
			roomname = self.get_user_input_line(self._stdscr, dialogue_y, 0, "Enter name of chatroom:")
			flags = ROOM_NOFLAGS

			# No empty, public or already defined rooms
			if len(roomname) == 0 or roomname == GUI.CHATROOM_NAME_PUBLIC or roomname in self._chatrooms:
				#TODO: show info message "Room already active"
				return True

			key = self.get_user_input_line(self._stdscr, dialogue_y, 0, "Enter password, blank=no pw:")

			if len(key) == 0:
				key = None
			else:
				logger.debug("Creating key from pw")
				key = get_normalized_aes_key(key)
				self._chatroom_key[roomname] = key
				logger.debug("key is: %s" % key)
				flags = ROOM_ENCRYPTED

			self._chatrooms[roomname] = [flags]
			self._chatroom_joined[0] = roomname
			self._chatroom_joined[1] = flags
			self._cb_switch_room(roomname, key)
			return True
		elif key == curses.KEY_F6:
			roomname = self._chatroom_joined[0]

			if roomname is None:
				# TODO: show hint "Need to join/create room first!"
				return False

			if self._chatroom_joined[1] != ROOM_ENCRYPTED:
				#TODO: show hint "No encryption needed for this room."
				return False

			key = self.get_user_input_line(self._stdscr, dialogue_y, 0, "Enter password for chatroom:")

			if len(key) == 0:
				return True

			key = get_normalized_aes_key(key)
			self._chatroom_key[roomname] = key
			self._cb_switch_room(roomname, key)
		elif key == 27:
			self._stop()
		return False


	def _redraw_mainscreen(self, repeats=1, force_refresh=False):
		self._redraw_main_needed = False
		self._stdscr.erase()

		if force_refresh:
			self._stdscr.refresh()

		self._draw_menu()
		self._draw_micsense()
		self._draw_info()
		self._draw_chatrooms()

		self._stdscr.refresh()

"""
def cb_switch_channel(channel):
	pass

def cb_switch_room(roomname, key):
	pass

def change_cycler(obj):
	cnt = 0
	crooms = OrderedDict()

	while obj._is_running:
		obj.update_nw_info(cnt, cnt+1000)
		obj.update_miclevel((cnt%11)/10)
		cnt = (cnt+1) & 0xFFFF

		if cnt % 100 == 0:
			if cnt > 20000:
				crooms.clear()
			crooms["Room_%d" % cnt] = [ROOM_NOFLAGS if cnt % 20 == 0 else ROOM_ENCRYPTED]
			obj.replace_chatrooms(crooms)
		time.sleep(0.1)
"""

"""
gui = GUI(
	cb_switch_channel=cb_switch_channel,
	cb_switch_room=cb_switch_room
)
gui.start()

th_changes = threading.Thread(target=change_cycler, args=[gui])
th_changes.start()

gui.waitforexit()
"""
"""
try:
	time.sleep(990.0001)
except:
	pass
"""

#print("After sleep")
#gui.stop()
#print("Stopped")
#th_changes.join()
