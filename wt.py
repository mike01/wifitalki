import logging
import argparse
import sys
import time

import audio_inout
import nw
import gui

from pypacker import utils

logger = logging.getLogger("wt")
logger.setLevel(logging.WARNING)

"""
> Local record
python wt.py -i wlp0s29f7u1 -d sysdefault -e sysdefault --logs logs.txt  --localrecord
python wt.py -i wlp0s29f7u1 --logs logs.txt --localrecord


> Local test: sending
python wt.py -i wlp2s0 -d sysdefault -e sysdefault --logs logs.txt --norcv

> Local test: receiving
python wt.py -i wlp0s29f7u1 -d sysdefault -e sysdefault --logs logs2.txt --nosend
"""
def enable_verbose_logging(logger, filename_logs):
	logger.setLevel(logging.DEBUG)

	#logger_handler = logging.StreamHandler()
	logger_handler = logging.FileHandler(filename_logs)
	#logger_formatter = logging.Formatter("%(levelname)s (%(funcName)s): %(message)s")
	logger_formatter = logging.Formatter("[%(funcName)s]: %(message)s")
	logger_handler.setFormatter(logger_formatter)

	logger.addHandler(logger_handler)


if __name__ != "__main__":
	sys.exit(1)

# python wt.py -i wlp2s0 -d default -e default
argparser = argparse.ArgumentParser(
	description="wt",
	formatter_class=argparse.RawDescriptionHelpFormatter
	)

argparser.add_argument(
	"-i", "--iface", type=str,
                help="Interface name",
		required=True,
        )

argparser.add_argument(
	"-d", "--audioin", type=str,
                help="Audio-in device name shown in device table (needs -e)",
		required=False,
		default=None
        )

argparser.add_argument(
	"-e", "--audioout", type=str,
                help="Audio-out device name shown in device table (needs -d)",
		required=False,
		default=None
        )

argparser.add_argument(
	"-l", "--logs", type=str,
                help="Enable logging to a logfile",
		required=False,
		default=None
	)

argparser.add_argument(
	"--propagation",
		help="Allow propagation of packets from the same room",
		required=False,
		default=True,
		action="store_true"
	)

argparser.add_argument(
	"--resents", type=int,
                help="Amount of resents to compensate for packet drops. # Packets sent = 1 + resents (mainly for wifi sticks)",
		required=False,
		default=0
	)

argparser.add_argument(
	"--norcv",
                help="Do not receive frames (for debugging)",
		required=False,
		default=False,
		action="store_true"
	)

argparser.add_argument(
	"--nosend",
                help="Do not send frames (for debugging)",
		required=False,
		default=False,
		action="store_true"
	)

argparser.add_argument(
	"--localrecord",
                help="Record frames locally and replay at program exit (for debugging)",
		required=False,
		default=False,
		action="store_true"
	)


args = argparser.parse_args()

if args.logs is not None:
	enable_verbose_logging(logger, args.logs)



FRAMELEN_MAX = 2200
# TODO: check this
MICLEVEL_MAX = int(0xFFFF/8)

nw.prepare_interface_for_monitor(args.iface)

odm = audio_inout.OutputMerger(args.audioout)


rec_frames = []

#
# NW
#
def cb_data_rcvd(id, cnt, room, frames):
	#TODO: activate
	if not args.norcv:
		#logger.debug("Got data from id=%X, %d, %s: %s ... %s" % (id, cnt, room, frames[:4], frames[-4:]))
		odm.queue_to_output(id, frames)
		#logger.debug("Finished queue_to_output")
	#if not args.norcv:
	#	logger.debug("Got data from id=%X, %d, %s: %s ... %s" % (id, cnt, room, frames[:4], frames[-4:]))
	#	rec_frames.append(frames)

def cb_update_rooms(rooms_seen):
	gui.update_chatrooms(rooms_seen)

# Set initial channel
INITIAL_CHANNEL = 4
utils.switch_wlan_channel(args.iface, INITIAL_CHANNEL)
nw_handler = nw.NWHandler(args.iface,
	cb_data_rcvd,
	resents=args.resents,
	cb_update_rooms=cb_update_rooms
)

#
# GUI
#
def cb_switch_channel(channel):
	utils.switch_wlan_channel(args.iface, channel)
	nw_handler.clear_roominfo()

def cb_switch_room(roomname, key):
	nw_handler.set_room(roomname)
	nw_handler.set_key(key)

def cb_update_miclevel_send_percent(miclevel):
	audioin.set_threshold(miclevel*MICLEVEL_MAX)

def cb_get_nw_info():
	nw_handler.get_nw_info()

gui = gui.GUI(
	initial_channel=INITIAL_CHANNEL,
	cb_switch_channel=cb_switch_channel,
	cb_switch_room=cb_switch_room,
	cb_update_miclevel_send_percent=cb_update_miclevel_send_percent,
	cb_get_nw_info=cb_get_nw_info
)

def cb_record(frames_bts, avg, threshold_reached):
	# TODO: remove
	#rec_frames.append(frames_bts)
	#return
	if threshold_reached:
		if args.localrecord:
			logger.debug("Recording %d bytes" % len(frames_bts))
			rec_frames.append(frames_bts)
		# TODO: activate
		elif not args.nosend:
			nw_handler.send(frames_bts)
	else:
		# Avoid: Data1 -> silence -> Data1 (buffered) -> Data2
		nw_handler._clear_sendbuffer()
	gui.update_miclevel(avg/MICLEVEL_MAX)

if not args.nosend:
	audioin = audio_inout.AudioIn(args.audioin, cb_record)
	# Apply initial level in gui to audio in
	audioin.set_threshold(gui._miclevel_send_percent*MICLEVEL_MAX)

gui.start()
gui.waitforexit()

if not args.nosend:
	audioin.stop()
nw_handler.stop()


if args.localrecord:
	logger.debug("Output of localrecord: %d bytes" % len(b"".join(rec_frames)))
	# TODO: queued frames need to be of size of requested length (in callback)
	for frame in rec_frames:
		odm.queue_to_output("xxx", frame)
	time.sleep(10)

#logger.debug("Saving to file")
#audio_inout.save_to_wave([b"".join(rec_frames)], "record.wav")
odm.stop()

